#!/usr/bin/env bash

# Create ENV variable for the installers to use
export DOTFILES_DIR="$(pwd)"

# Installers
installers=(
  brew.sh
  brew-cask.sh
  brew-cask-fonts.sh
  macos/defaults.sh
  macos/dock.sh
  zsh.sh
  rbenv.sh
  postgresql.sh
  symlinks.sh
)
. "$DOTFILES_DIR/install/${installers[@]}"

sleep 1
echo "All installers have finished!"
