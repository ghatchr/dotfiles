#!/usr/bin/env bash

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Update & Upgrade
brew update
brew upgrade

# Install Packages
packages=(
  coreutils
  moreutils
  findutils
  dockutil
  htop
  iftop
  openssl
  nmap
  git
  python3
  thefuck
  wget --with-iri
  webkit2png
  sqlmap
  aircrack-ng
  unrar
  wifi-password
  gnu-sed --with-default-names
  grep --with-default-names
  rename
  tree
  imagemagick --with-webp
  ssh-copy-id
  httpie
  redis
)
brew install "${packages[@]}"

sleep 1
echo "Finished installing command-line tools via Homebrew"
