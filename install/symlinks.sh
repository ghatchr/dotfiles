#!/usr/bin/env bash

# Bash
ln -sfv "$DOTFILES_DIR/dots/.bashrc" ~

# Hyper.app
ln -sfv "$DOTFILES_DIR/dots/.hyper.js" ~

# Git
ln -sfv "$DOTFILES_DIR/dots/.gitconfig" ~
ln -sfv "$DOTFILES_DIR/dots/.gitignore" ~
