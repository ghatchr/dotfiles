#!/usr/bin/env bash

# Install Casks for Homebrew
brew tap caskroom/cask
brew tap caskroom/versions

# Install Applications
applications=(
  atom
  google-chrome
  marshallofsound-google-play-music-player
  slack
  skype
  hyper
  1password
  gitkraken
  microsoft-office
)
brew cask install "${applications[@]}"

# Quick Look Plugins (https://github.com/sindresorhus/quick-look-plugins)
 qlcolorcode qlstephen qlmarkdown quicklook-json qlprettypatch quicklook-csv qlimagesize webpquicklook qlvideo

sleep 1
echo "Finished installing applications via Homebrew Cask"
