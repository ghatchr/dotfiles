#!/usr/bin/env bash

# Install rbenv and ruby-build
brew install rbenv
brew install ruby-build

# Create Symlinks
ln -sfv "$DOTFILES_DIR/dots/.gemrc" ~

# Download Ruby
rbenv install 2.5.1
rbenv global 2.5.1

# Install Gems
gems=(
  bundler
)
gem install "$gems[@]"

sleep 1
echo "Finished installing Ruby / Gems"
