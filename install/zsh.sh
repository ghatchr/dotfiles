#!/usr/bin/env bash

# Install Zsh
brew install zsh
brew install zsh-completions

# Install Oh My Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Add Zsh to Shell
sudo sh -c "echo $(which zsh) >> /etc/shells"
chsh -s $(which zsh)

# Create Symlinks
ln -sfv "$DOTFILES_DIR/oh-my-zsh/themes/granth.zsh-theme" ~/.oh-my-zsh/themes

sleep 1
echo "Finished installing Oh My Zsh"
