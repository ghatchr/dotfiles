#!/usr/bin/env bash

# Install Cask Fonts for Homebrew
brew tap homebrew/cask-fonts

# Install Fonts
fonts=(
  font-inconsolata
  font-inconsolata-for-powerline
  font-inconsolata-dz-for-powerline
  font-inconsolata-g-for-powerline
  font-fira-code
)
brew cask install "${fonts[@]}"

sleep 1
echo "Finished installing fonts via Homebrew Cask Fonts"
