#!/usr/bin/env bash

# Install command line tools so we can use git right off the bat
xcode-select --install

# Create the code directory
mkdir -p "$HOME/Code/granth"
cd "$HOME/Code/granth"

# Clone the `dotfiles` repository
git clone https://gitlab.com/granth/dotfiles.git
